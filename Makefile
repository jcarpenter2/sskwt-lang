# http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/

DEPDIR := .d
$(shell mkdir -p $(DEPDIR) >/dev/null)
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
POSTCOMPILE= @mv -f $(DEPDIR)/$*.Td $(DEPDIR)/obj/$*.d && touch $@

CXX=g++
CXXFLAGS=$(DEPFLAGS) -std=c++17 -g
LDFLAGS=-ldl

find_files = $(wildcard $(dir)/*.cpp)
dirs := ./src ./src/blocks

source := $(foreach dir,$(dirs),$(find_files))
objects=$(patsubst ./src/%.cpp,./obj/%.o,$(source))

$(objects): ./obj/%.o: ./src/%.cpp
	@ mkdir -p .d/$(dir $@)
	@ mkdir -p obj/$(dir $(@:obj/%=%))
	$(CXX) -I./src $(CXXFLAGS) -c $< -o $@
	$(POSTCOMPILE)

sskwt: $(objects)
	$(CXX) $(objects) $(LDFLAGS) -o sskwt

$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

clean:
	rm -rf obj

include $(patsubst %,$(DEPDIR)/%.d,$(basename $(objects)))
