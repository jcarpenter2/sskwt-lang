#ifndef PARSER_H
#define PARSER_H

#include "expected.hpp"

using ParseError = std::string;

template<typename T>
using ParseResult = nonstd::expected<T, ParseError>;

template<typename T>
ParseResult<T>
parse_error(std::string message) {
  return nonstd::make_unexpected<T>(message);
}

ParseResult<char>
parse_char(std::string chars, std::string_view& source) {
  if (chars.find_first_of(source[0]) != std::string::npos) {
    auto ch = source[0];
    source = source.substr(1);
    return ch;
  }
  return nonstd::make_unexpected<std::string>("Char not found: " + chars);
}

ParseResult<char>
parse_char_inv(std::string chars, std::string_view& source) {
  if (chars.find_first_of(source[0]) == std::string::npos) {
    auto ch = source[0];
    source = source.substr(1);
    return ch;
  }
  return nonstd::make_unexpected<std::string>("Char was one of: " + chars);
}

ParseResult<std::string>
parse_chars(std::string chars, std::string_view &source) {
  auto pos = source.find_first_not_of(chars);
  if (pos == 0) {
    return nonstd::make_unexpected<std::string>("Chars not found: " + chars);
  }
  auto result = std::string(source.substr(0, pos));
  source = source.substr(std::min(pos, source.size()));
  return result;
}

ParseResult<std::string>
parse_chars_inv(std::string chars, std::string_view &source) {
  auto pos = source.find_first_of(chars);
  if (pos == 0) {
    return nonstd::make_unexpected<std::string>("First char was one of: " + chars);
  }
  auto result = std::string(source.substr(0, pos));
  source = source.substr(std::min(pos, source.size()));
  return result;
}

ParseResult<std::string>
parse_string(std::string str, std::string_view &source) {
  if (str == source.substr(0, str.size())) {
    source = source.substr(str.size());
    return str;
  }
  return nonstd::make_unexpected<std::string>("String not found: " + str);
}

#endif
