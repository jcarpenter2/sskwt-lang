#include <dlfcn.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cstring>
#include <functional>
#include <iostream>
#include <memory>
#include <optional>
#include <string_view>
#include <unordered_map>
#include <variant>
#include <vector>

#include "parser.h"

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

struct Double;
struct String;
struct FunctionCall;
struct Variable;
struct FunctionDef;
using Expression = std::variant<
  Double,
  String,
  FunctionCall,
  Variable,
  std::shared_ptr<FunctionDef>
>;
struct Double {
  double val;
};
struct String {
  std::string str;
};
struct FunctionCall {
  std::shared_ptr<Expression> function_name;
  std::vector<std::shared_ptr<Expression>> arguments;
};
struct Variable {
  std::string name;
};

struct Import;
struct Assignment;
using Statement = std::variant<
  Import,
  Assignment,
  Expression
>;
struct Import {
  struct ImportSymbol {
    std::string name;
    std::string return_type;
    std::vector<std::string> arg_types;
  };
  std::string so_name;
  std::vector<ImportSymbol> import_symbols;
};
struct Assignment {
  std::string variable;
  Expression expr;
};

struct CppFunction;
struct Symbol;
struct Function;
using Value = std::variant<
  double,
  void*,
  CppFunction,
  Symbol,
  std::shared_ptr<Function>,
  std::shared_ptr<std::string>
>;
struct CppFunction {
  std::string name;
  std::function<Value(std::vector<Value> const&)> func;
};
struct Symbol {
  enum class Type {
    Int,
    Double,
    CharPointer,
    Pointer,
  };
  std::string name;
  Type result;
  std::vector<Type> arguments;
  void *fn;
  void (*martial)(void*, void*, void*);
};
struct Function {
  std::vector<std::optional<Symbol::Type>> types;
  std::vector<std::string> arguments;
  std::vector<Statement> stmts;
  void *landing_pad;
};

struct FunctionDef {
  std::vector<std::optional<std::string>> types;
  std::vector<std::string> arguments;
  std::vector<Statement> stmts;
};

std::ostream& operator<<(std::ostream& out, Expression const& expr) {
  std::visit(overloaded {
    [&out](Double const& d) {
      out << d.val;
    },
    [&out](String const& str) {
      out << str.str;
    },
    [&out](FunctionCall const& f) {
      out << *f.function_name << "(";
      for (auto const& expr : f.arguments) {
        out << *expr;
      }
      out << ")";
    },
    [&out](Variable const& var) {
      out << var.name;
    },
    [&out](std::shared_ptr<FunctionDef> const&) {
      out << "FunctionDef";
    }
  }, expr);
  return out;
}

std::ostream& operator<<(std::ostream& out, Value const& val) {
  std::visit(overloaded {
    [&out](double d) {
      out << d;
    },
    [&out](void* ptr) {
      out << "Pointer: " << ptr;
    },
    [&out](CppFunction const& cpp_func) {
      out << "Built-in Function: " << cpp_func.name;
    },
    [&out](Symbol const& sym) {
      out << "Symbol: " << sym.name;
    },
    [&out](std::shared_ptr<Function> const& func) {
      out << "Function";
    },
    [&out](std::shared_ptr<std::string> const& str) {
      out << *str;
    },
  }, val);
  return out;
}

using Name = std::string;
struct Context {
  std::vector<std::unordered_map<Name, Value>> names;
};
// global context needed for eval_function()
auto ctx = Context {};

Value const* find_expression(Context& ctx, std::string name) {
  for (auto const& map : ctx.names) {
    if (map.count(name)) {
      return &map.at(name);
    }
  }
  return nullptr;
}

Symbol::Type
parse_symbol_type(std::string const& str) {
  if (str == "int") {
    return Symbol::Type::Int;
  }
  if (str == "double") {
    return Symbol::Type::Double;
  }
  if (str == "char*") {
    return Symbol::Type::CharPointer;
  }
  if (str == "void*") {
    return Symbol::Type::Pointer;
  }
  else {
    std::cout << "Unknown type: " << str << std::endl;
    exit(1);
  }
}

void exec(Statement const& stmt, Context &ctx);
Value eval_function(Function const& func, std::vector<Value> const& values) {
  ctx.names.insert(ctx.names.begin(), std::unordered_map<Name, Value> {});
  if (func.arguments.size() != values.size()) {
    std::cout << "Wrong number of arguments passed to function call" << std::endl;
    exit(1);
  }
  for (size_t i = 0; i < values.size(); i++) {
    ctx.names[0][func.arguments[i]] = values[i];
  }
  for (auto const& stmt : func.stmts) {
    exec(stmt, ctx);
  }
  ctx.names.erase(ctx.names.begin());
  return 0.0;
}

void eval_from_c_args(char *args, Function *function) {
  std::vector<Value> values;
  for (auto type : function->types) {
    if (!type) {
      std::cout << "The impossible happened, a C-interface function is missing a type" << std::endl;
      exit(1);
    }
    switch (*type) {
    case Symbol::Type::Int: {
      int64_t x = 0;
      memcpy(&x, args, sizeof(x));
      values.push_back(Value{(double)x});
      break;
    }
    case Symbol::Type::Double: {
      double d = 0;
      memcpy(&d, args, sizeof(d));
      values.push_back(Value{d});
      break;
    }
    case Symbol::Type::Pointer: {
      void *ptr = nullptr;
      memcpy(&ptr, args, sizeof(ptr));
      values.push_back(Value{ptr});
      break;
    }
    default:
      std::cout << "Callback argument type not handled" << std::endl;
      exit(1);
    }
    args += 8;
  }
  eval_function(*function, values);
}

void *make_landing_pad(std::vector<Symbol::Type> const& types, Function* function) {
  std::vector<char> code;
  size_t args_size = 0;
  for (auto type : types) {
    switch (type) {
    case Symbol::Type::Int:
    case Symbol::Type::Double:
    case Symbol::Type::CharPointer:
    case Symbol::Type::Pointer:
      args_size += 8;
      break;
    }
  }

  // enter args_size,0
  if (args_size > 0xff) {
    std::cout << "Args size too large" << std::endl;
    exit(1);
  }
  code.push_back(0xc8);
  code.push_back(args_size);
  code.push_back(0x00);
  code.push_back(0x00);

  size_t rsp_offset = 0;
  size_t int_index = 0;
  size_t double_index = 0;

  for (auto type : types) {
    bool again = false;
    switch (type) {
    case Symbol::Type::Int: {
    case Symbol::Type::CharPointer:
    case Symbol::Type::Pointer:
      if (int_index > 5) {
        std::cout << "int index too large" << std::endl;
        exit(1);
      }
      // mov [rsp+rsp_offset], (register)
      if (int_index < 4) {
        code.push_back(0x48);
      }
      else {
        code.push_back(0x4c);
      }
      code.push_back(0x89);
      char reg_byte[] = {
        0x7c,
        0x74,
        0x54,
        0x4c,
        0x44,
        0x4c,
      };
      code.push_back(reg_byte[int_index]);
      code.push_back(0x24);
      code.push_back(rsp_offset);
      int_index++;
      rsp_offset += 8;
      break;
    }
    case Symbol::Type::Double:
      std::cout << "Double callback argument type not supported yet" << std::endl;
      exit(1);
      if (double_index > 5) {
        std::cout << "double index too large" << std::endl;
        exit(1);
      }
      break;
    default:
      std::cout << "Unspported callback argument type" << std::endl;
      exit(1);
    }
  }

  // mov rdi, rsp
  code.push_back(0x48);
  code.push_back(0x89);
  code.push_back(0xe7);

  // mov rsi, function
  code.push_back(0x48);
  code.push_back(0xbe);
  for (size_t i = 0; i < 8; i++){
    code.push_back(((char*)&function)[i]);
  }

  // mov rdx, eval_from_c_args
  void *eval_from_c_args_ptr = (void*)&eval_from_c_args;
  code.push_back(0x48);
  code.push_back(0xba);
  for (size_t i = 0; i < 8; i++){
    code.push_back(((char*)&eval_from_c_args_ptr)[i]);
  }

  // call rdx
  code.push_back(0xff);
  code.push_back(0xd2);

  // leave
  code.push_back(0xc9);

  // ret
  code.push_back(0xC3);

  auto code_ptr = mmap(nullptr, code.size(), PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (code_ptr == MAP_FAILED) {
    std::cout << "Landing pad map failed" << std::endl;
    exit(1);
  }
  memcpy(code_ptr, code.data(), code.size());
  return code_ptr;
}

Value eval(Expression const& expr, Context &ctx) {
  return std::visit(overloaded {
    [](Double const& d) -> Value {
      return d.val;
    },
    [](String const& str) -> Value {
      return std::make_shared<std::string>(str.str);
    },
    [&ctx](FunctionCall const& fn_call) -> Value {
      auto args = std::vector<Value> {};
      for (auto const& arg : fn_call.arguments) {
        args.push_back(eval(*arg, ctx));
      }
      auto expr = eval(*fn_call.function_name, ctx);
      if (auto cpp_fn = std::get_if<CppFunction>(&expr)) {
        return cpp_fn->func(args);
      }
      else if (auto func = std::get_if<std::shared_ptr<Function>>(&expr)) {
        return eval_function(**func, args);
      }
      else if (auto sym = std::get_if<Symbol>(&expr)) {
        if (sym->arguments.size() != args.size()) {
          std::cout << "Wrong number of arguments passed to function call: " << sym->name << std::endl;
          exit(1);
        }
        if (args.size() > 6) {
          std::cout << "More than 6 args not supported" << std::endl;
        }
        std::vector<char> c_args;
        for (size_t i = 0; i < args.size(); i++) {
          auto arg_type = sym->arguments[i];
          auto const& arg = args[i];
          switch (arg_type) {
          case Symbol::Type::Int:
            if (auto d = std::get_if<double>(&arg)) {
              int64_t x = *d;
              size_t index = c_args.size();
              c_args.resize(c_args.size() + sizeof(x));
              memcpy(&c_args[index], &x, sizeof(x));
              continue;
            }
          case Symbol::Type::Double:
            if (auto d = std::get_if<double>(&arg)) {
              size_t index = c_args.size();
              c_args.resize(c_args.size() + sizeof(*d));
              memcpy(&c_args[index], d, sizeof(*d));
              continue;
            }
            break;
          case Symbol::Type::CharPointer:
            if (auto str = std::get_if<std::shared_ptr<std::string>>(&arg)) {
              auto c_str = (*str)->data();
              size_t index = c_args.size();
              c_args.resize(c_args.size() + sizeof(c_str));
              memcpy(&c_args[index], &c_str, sizeof(c_str));
              continue;
            }
            break;
          case Symbol::Type::Pointer:
            if (auto ptr = std::get_if<void*>(&arg)) {
              size_t index = c_args.size();
              c_args.resize(c_args.size() + sizeof(*ptr));
              memcpy(&c_args[index], ptr, sizeof(*ptr));
              continue;
            }
            else if (auto str = std::get_if<std::shared_ptr<std::string>>(&arg)) {
              auto c_str = (*str)->data();
              size_t index = c_args.size();
              c_args.resize(c_args.size() + sizeof(c_str));
              memcpy(&c_args[index], &c_str, sizeof(c_str));
              continue;
            }
            else if (auto n = std::get_if<double>(&arg)) {
              void *ptr;
              memcpy(&ptr, n, sizeof(ptr));
              size_t index = c_args.size();
              c_args.resize(c_args.size() + sizeof(ptr));
              memcpy(&c_args[index], &ptr, sizeof(ptr));
              continue;
            }
            else if (auto func = std::get_if<std::shared_ptr<Function>>(&arg)) {
              void* ptr = (*func)->landing_pad;
              if (!ptr) {
                std::cout << "Cannot use non-C-interface function as callback" << std::endl;
                exit(1);
              }
              size_t index = c_args.size();
              c_args.resize(c_args.size() + sizeof(ptr));
              memcpy(&c_args[index], &ptr, sizeof(ptr));
              continue;
            }
          }
          std::cout << "Could not coerce argument type" << std::endl;
          exit(1);
        }
        switch (sym->result) {
        case Symbol::Type::Int: {
          int result;
          auto data_ptr = c_args.data();
          sym->martial(data_ptr, sym->fn, &result);
          return (double)result;
        }
        case Symbol::Type::Double: {
          double result;
          auto data_ptr = c_args.data();
          sym->martial(data_ptr, sym->fn, &result);
          return result;
        }
        case Symbol::Type::Pointer: {
          void *result;
          auto data_ptr = c_args.data();
          sym->martial(data_ptr, sym->fn, &result);
          return result;
        }
        default:
          std::cout << "Could not coerce return type" << std::endl;
          exit(1);
        }
      }
      std::cout << "Not a function: " << *fn_call.function_name << std::endl;
      exit(1);
    },
    [&ctx](std::shared_ptr<FunctionDef> const& function_def) -> Value {
      std::vector<std::optional<Symbol::Type>> types;
      for (auto const& type : function_def->types) {
        if (type) {
          types.push_back(parse_symbol_type(*type));
        }
        else {
          types.push_back({});
        }
      }
      bool do_landing_pad = true;
      std::vector<Symbol::Type> all_types;
      for (auto const& type : types) {
        if (type) {
          all_types.push_back(*type);
        }
        else {
          do_landing_pad = false;
        }
      }
      void *landing_pad = nullptr;
      auto result = std::make_shared<Function>(Function {
        std::move(types),
        function_def->arguments,
        function_def->stmts,
      });
      if (do_landing_pad) {
        landing_pad = make_landing_pad(all_types, &*result);
      }
      result->landing_pad = landing_pad;
      return result;
    },
    [&ctx](Variable const& var) -> Value {
      auto val = find_expression(ctx, var.name);
      if (!val) {
        std::cout << "No such variable: " << var.name << std::endl;
        exit(1);
      }
      return *val;
    },
    [](auto const& _) -> Value {
      std::cout << "Eval not implemented" << std::endl;
      exit(1);
    }
  }, expr);
}

void exec(Statement const& stmt, Context &ctx) {
  std::visit(overloaded {
    [&ctx](Import const& import) {
      auto handle = dlopen(import.so_name.data(), RTLD_LAZY);
      if (!handle) {
        std::cout << "Could not open file: " << import.so_name << std::endl;
        exit(1);
      }
      for (auto const& import_symbol : import.import_symbols) {
        auto return_type = parse_symbol_type(import_symbol.return_type);
        auto sym = Symbol {
          import_symbol.name,
          return_type,
        };
        std::vector<char> code;
        size_t arg_n = 0;
        size_t integer_n = 0;
        size_t double_n = 0;
        // mov rax, rdi
        code.push_back(0x48);
        code.push_back(0x89);
        code.push_back(0xf8);
        // mov r10, rsi
        code.push_back(0x49);
        code.push_back(0x89);
        code.push_back(0xf2);
        // mov r11, rdx
        code.push_back(0x49);
        code.push_back(0x89);
        code.push_back(0xd3);
        for (auto const& arg_type : import_symbol.arg_types) {
          auto type = parse_symbol_type(arg_type);
          sym.arguments.push_back(type);
          switch (type) {
          case Symbol::Type::Double: {
            if (double_n > 15) {
              std::cout << "Too many double arguments" << std::endl;
              exit(1);
            }
            // movq xmm(double_n), [rdi+(arg_n)]
            code.push_back(0xF3);
            code.push_back(0x0F);
            code.push_back(0x7E);
            char from_rdi = 0x40;
            from_rdi += 8 * double_n++;
            code.push_back(from_rdi);
            code.push_back(8 * arg_n++);
            break;
          }
          case Symbol::Type::Int:
          case Symbol::Type::Pointer:
          case Symbol::Type::CharPointer: {
            char third_bit[] = {
              0x78, // rdi
              0x70, // rsi
              0x50, // rdx
              0x48, // rcx
              0x40, // r8
              0x48, // r9
            };
            if (integer_n > 5) {
              std::cout << "Too many integer arguments" << std::endl;
              exit(1);
            }
            // mov (dst register)
            if (integer_n < 4) {
              code.push_back(0x48);
            }
            else {
              code.push_back(0x4c);
            }
            code.push_back(0x8b);
            code.push_back(third_bit[integer_n++]);
            code.push_back(8 * arg_n++);
            break;
          }
          default:
            std::cout << "Argument type not handled" << std::endl;
            exit(1);
          }
        }
        // push r11
        code.push_back(0x41);
        code.push_back(0x53);
        // call r10
        code.push_back(0x41);
        code.push_back(0xFF);
        code.push_back(0xD2);
        // pop r11
        code.push_back(0x41);
        code.push_back(0x5b);
        switch (return_type) {
        case Symbol::Type::Int:
        case Symbol::Type::Pointer:
          // mov [r11], rax
          code.push_back(0x49);
          code.push_back(0x89);
          code.push_back(0x03);
          break;
        case Symbol::Type::Double:
          // movq [r11], xmm0
          code.push_back(0x66);
          code.push_back(0x41);
          code.push_back(0x0f);
          code.push_back(0xd6);
          code.push_back(0x03);
          break;
        default:
          std::cout << "Return type not handled" << std::endl;
          exit(1);
        }
        // ret
        code.push_back(0xC3);
        auto code_ptr = mmap(nullptr, code.size(), PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (code_ptr == MAP_FAILED) {
          std::cout << "Executable map failed" << std::endl;
          exit(1);
        }
        memcpy(code_ptr, code.data(), code.size());
        auto obj = dlsym(handle, import_symbol.name.data());
        if (!obj) {
          std::cout << "No such object " + import_symbol.name + " in file " + import.so_name << std::endl;
          exit(1);
        }
        sym.fn = obj;
        sym.martial = (void (*)(void*, void*, void*))code_ptr;
        ctx.names[0][import_symbol.name] = sym;
      }
    },
    [&ctx](Assignment const& assignment) {
      ctx.names[0][assignment.variable] = eval(assignment.expr, ctx);
    },
    [&ctx](Expression const& expr) {
      eval(expr, ctx);
    },
  }, stmt);
}

std::string variable_chars_inv = "() \t\n,;";

Statement
parse_stmt(std::string_view &source);

Expression
parse_expr(std::string_view &source) {
  if (!source.size()) {
    std::cout << "Unexpected end of code" << std::endl;
    exit(1);
  }
  if (parse_char("\"", source)) {
    std::string result;
    while (auto ch = parse_char_inv("\"", source)) {
      if (*ch == '\\') {
        if (ch = parse_char_inv("", source)) {
          result.push_back(*ch);
        }
        continue;
      }
      result.push_back(*ch);
    }
    parse_char("\"", source);
    return String { result };
  }
  if (parse_string("function", source)) {
    parse_chars(" \t\n", source);
    if (!parse_char("(", source)) {
      std::cout << "Need (" << std::endl;
      exit(1);
    }
    parse_chars(" \t\n", source);
    std::vector<std::optional<std::string>> types;
    std::vector<std::string> arguments;
    while (!parse_char(")", source)) {
      auto argument = parse_chars_inv(variable_chars_inv, source);
      if (!argument) {
        std::cout << "Not an argument!" << std::endl;
        exit(1);
      }
      arguments.push_back(*argument);
      parse_chars(" \t\n", source);
      if (0 == source.find_first_not_of(",)")) {
        auto type = parse_chars_inv(variable_chars_inv, source);
        if (!type) {
          std::cout << "Not a type!" << std::endl;
          exit(1);
        }
        types.push_back(*type);
      }
      else {
        types.push_back({});
      }
      parse_chars(", \t\n", source);
    }
    parse_chars(" \t\n", source);
    if(!parse_char("{", source)) {
      std::cout << "Need {" << std::endl;
      exit(1);
    }
    parse_chars(" \t\n", source);
    std::vector<Statement> stmts;
    while (!parse_char("}", source)) {
      stmts.push_back(parse_stmt(source));
      parse_chars(" \t\n", source);
    }
    return std::make_shared<FunctionDef>(FunctionDef {
      std::move(types),
      std::move(arguments),
      std::move(stmts)
    });
  }
  if (auto str = parse_chars("0123456789.", source)) {
    return Double {
      std::stod(*str),
    };
  }
  auto str = parse_chars_inv(variable_chars_inv, source);
  if (parse_char("(", source)) {
    auto fn_call = FunctionCall {
      std::make_shared<Expression>(Variable {
        std::string(*str),
      }),
    };
    while (!parse_char(")", source)) {
      fn_call.arguments.push_back(std::make_shared<Expression>(parse_expr(source)));
      parse_chars(", \t\n", source);
    }
    return fn_call;
  }
  return Variable { std::string(*str) };
}

Statement
parse_stmt(std::string_view &source) {
  Statement result;
  if (parse_string("import", source)) {
    parse_chars(" \t\n", source);
    auto so_name = parse_chars_inv(" \t\n", source);
    parse_chars(" \t\n", source);
    if (!parse_char("{", source)) {
      std::cout << "Not importing anything from import statement!" << std::endl;
      exit(1);
    }
    parse_chars(" \t\n", source);
    auto import = Import {
      *so_name,
    };
    while (!parse_char("}", source)) {
      auto type = parse_chars_inv(" \t\n", source);
      parse_chars(" \t\n", source);
      auto name = parse_chars_inv(" \t\n(", source);
      parse_char("(", source);
      auto args = std::vector<std::string> {};
      while (auto arg = parse_chars_inv(" \t\n,)", source)) {
        args.push_back(*arg);
        parse_chars(" \t\n,", source);
      }
      parse_char(")", source);
      parse_chars(", \t\n", source);
      import.import_symbols.push_back(Import::ImportSymbol {
        *name,
        *type,
        args,
      });
    }
    result = import;
  }
  else if (parse_string("=", source)) {
    parse_chars(" \t\n", source);
    auto var = parse_chars_inv(variable_chars_inv, source);
    if (!var) {
      std::cout << "No variable in assignment!" << std::endl;
      exit(1);
    }
    parse_chars(" \t\n", source);
    auto expr = parse_expr(source);
    result = Assignment {
      *var,
      std::move(expr),
    };
  }
  else {
    result = parse_expr(source);
  }
  parse_chars(" \t\n", source);
  if (!parse_char(";", source)) {
    std::cout << "Expected ';'" << std::endl;
    exit(1);
  }
  return result;
}

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cout << "Please enter source file." << std::endl;
    return 1;
  }
  int fd = open(argv[1], O_RDONLY);
  if (fd == -1) {
    std::cout << "Could not open file!" << std::endl;
    return 1;
  }
  struct stat stat_struct;
  if (-1 == fstat(fd, &stat_struct)) {
    std::cout << "Could not stat file!" << std::endl;
    return 1;
  }
  char *source_buf = (char*)mmap(nullptr, stat_struct.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  if (source_buf == (char*)-1) {
    std::cout << "Could not mmap file!" << std::endl;
    return 1;
  }
  auto source = std::string_view{source_buf, (size_t)stat_struct.st_size};
  auto stmts = std::vector<Statement>();
  parse_chars(" \t\n", source);
  while (source.size()) {
    stmts.push_back(parse_stmt(source));
    parse_chars(" \t\n", source);
  }
  auto initial_ctx = std::unordered_map<Name, Value> {};
  initial_ctx["print"] = CppFunction {
    "print",
    [](std::vector<Value> const& args) -> Value {
      for (auto const& arg : args) {
        std::cout << arg;
      }
      std::cout << std::endl;
      return 0.0;
    },
  };
  initial_ctx["string"] = CppFunction {
    "string",
    [](std::vector<Value> const& args) -> Value {
      if (args.size() != 2) {
        std::cout << "Wrong number of arguments to string() (expected 2)" << std::endl;
        exit(1);
      }
      auto ptr = std::get_if<void*>(&args[0]);
      if (!ptr) {
        std::cout << "First argument to string() not pointer" << std::endl;
        exit(1);
      }
      auto size = std::get_if<double>(&args[1]);
      if (!size) {
        std::cout << "Second argument to string() not size" << std::endl;
        exit(1);
      }
      return Value{std::make_shared<std::string>((char*)*ptr, (size_t)*size)};
    },
  };
  initial_ctx["+"] = CppFunction {
    "+",
    [](std::vector<Value> const& args) -> Value {
      double sum = 0.0;
      for (auto const& arg : args) {
        auto d = std::get_if<double>(&arg);
        if (!d) {
          std::cout << "+ expects numeric types" << std::endl;
          exit(1);
        }
        sum += *d;
      }
      return Value{sum};
    },
  };
  ctx.names.push_back(std::move(initial_ctx));
  for (auto const& stmt : stmts) {
    exec(stmt, ctx);
  }
  return 0;
}
