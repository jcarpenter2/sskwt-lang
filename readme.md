# SSKWT Language

Some Script Kiddie Wrote This Language

## Coding in SSKWT

A program is a series of statements ending in semicolons.  Here is the
Hello World program:

```
print("Hello World!");
```

All function calls are spelled `f(x)` with the function name first,
including operators.  Instead of `2 + 3` we write the function call
`+(2, 3)`.

```
print(+(2, 3));
```

Variables in scope can only be created through the assignment
statement beginning with `=`.  To assign the value `5` to the variable
`x` write:

```
= x 5;
```

Functions are values assigned to variables.  Return statements are not
implemented yet.  To define a function `f` that prints two values:

```
= f function (x, y) {
  print(x, y);
};
```

C interop is done through SSKWT `import` statements:

```
import libm.so.6 {
  int strlen(char*);
};
```

An import statement gives a shared object file, and then the functions
to import from it.  After importing functions, they can be used in
SSKWT code.

```
import libm.so.6 {
  double cos(double);
};

print(cos(3));
```

C libraries that call callbacks can be used by writing a C-interface
SSKWT function.  A C-interface function names the data types of the
arguments.

In this example `read_handler` takes four arguments with data types
specified, and it is called back by curl.  The `string` function
constructs a SSKWT string out of a pointer and a size.

```
import libcurl.so.4 {
  void* curl_easy_init(),
  int curl_easy_setopt(void*, int, void*),
  int curl_easy_perform(void*),
};

= CURLOPT_URL 10002;
= CURLOPT_WRITEFUNCTION 20011;

= read_handler function (data void*, size int, nmemb int, userdata void*) {
  = str string(data, nmemb);
  print(str);
};

= curl curl_easy_init();
curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, read_handler);
curl_easy_perform(curl);
```

A few more examples are in the test file.

## Building and Running

```
$ make sskwt
$ ./sskwt test.sskwt
```

## Contributing

Pull requests are welcome.  I may merge them especially if they are
small, and I might add more commits on top changing stuff.  Bug
reports are preferable, since this code is pretty batty, and it
probably wouldn't be worth your while to try to read and understand
it.
